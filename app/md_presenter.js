
MDPresenter = {
    version:'1.1.0',
    mode:'deck',
    currentSlide:0,
    count:0,
    filename:'',
    hash_enabled:1,
    nav_scroll:1, // navigation arrows scroll down before next slide
}

//---Slide Generation
function loadSlides() {
    
    document.getElementById('slide_deck').innerHTML = ''
    
    // Get Slides
    let txt = document.getElementById('ta_presentation').value
    slidespl = txt.split('\n----')
    // Remove first slide if blank
    if (slidespl[0]=='') {
        slidespl.splice(0,1)
    } 
    // Remove last slide if blank
    if (slidespl.slice(-1)[0].trim() == '') {
        slidespl.pop(0)
        
    }
    
    // Parse Slides
    let slide_deck_elm = document.getElementById('slide_deck')
    slide_deck = []
    let cnt = 0
    for (slide of slidespl) {
        cnt++
        let title_elm
        
        // Outer Slide Shell
        let slide_elm = document.createElement('div')
        slide_elm.setAttribute('class','slide')
        slide_elm.setAttribute('ondblclick','viewSlide('+cnt+')')
        slide_elm.setAttribute('data-slide',cnt)
        
        // Inner Slide Box
        let slidebox_elm = document.createElement('div')
        slidebox_elm.setAttribute('class','slide-box')
        let cols = slide.split('\n||||')
        if (cols.length >1) {
            // Split columns
            slidebox_elm.setAttribute('class','slide-box cols')
            // Load Columns
            let c= -1
            for (col of cols) {
                c++
                let col_elm = document.createElement('div')
                col_elm.innerHTML = mdCompile(col)
                col_elm.setAttribute('class','col')
                slidebox_elm.appendChild(col_elm)
                if (c == 0) {
                    title_elm = col_elm.querySelector('h1')
                }
            }
        } else {
            // No column split
            slidebox_elm.innerHTML = mdCompile(slide)
            title_elm = slidebox_elm.querySelector('h1')
        }
        
        // Add Elements
        slide_elm.appendChild(slidebox_elm)
        slide_deck_elm.appendChild(slide_elm)
        
        // Setup Title
        if (title_elm) {
            title_elm.className='slide-title'
            slide_elm.insertBefore(title_elm,slidebox_elm)
        }
        
    }
    MDPresenter.count = cnt
    
    // Set Title
    let doc_title_h1 = document.querySelectorAll('h1')[0]
    if (doc_title_h1 !== undefined) {
        document.title = doc_title_h1.innerText
    }
    if (MDPresenter.filename == '') {
        MDPresenter.filename = document.title+'.md'
    }
    
    // remove existing footer
    let old_footer = document.querySelector('#slide_viewer footer')
    if (old_footer) {
        document.getElementById('slide_viewer').removeChild(old_footer)
    }
    // Add Footer
    let footer = document.querySelector('footer')
    if (footer) {
        document.getElementById('slide_viewer').append(footer)
    }
    
}

function replaceAll(string, search, replacement) {
    return string.split(search).join(replacement)
}

function mdCompile(txt) {
    // var cmk_parser = new commonmark.Parser()
    // var cmk_renderer = new commonmark.HtmlRenderer()
    // var parsed = cmk_parser.parse(txt)
    // result = cmk_renderer.render(parsed)
    result = cmarkdRender(txt)

    // Add some custom formatting
    // result = replaceAll(result,'<li>[ ]','<li class="checkbox"><input type="checkbox" disabled="disabled">')
    // result = replaceAll(result,'<li>[]','<li class="checkbox"><input type="checkbox" disabled="disabled">')
    // result = replaceAll(result,'<li>[x]','<li class="checkbox checked"><input type="checkbox" checked="checked" disabled="disabled">')
    // result = replaceAll(result,'<li>[-]','<li class="cancelled"><input type="checkbox" checked="checked" disabled="disabled">')
    // result = replaceAll(result,'<li>[f]','<li class="future"><input type="checkbox" disabled="disabled"> FUTURE')
    result = replaceAll(result,'<a href="http','<a target="_blank" href="http')
    
    return result
}


//---Presentation
function startPresentation() {
    if (MDPresenter.mode != 'present') {
        MDPresenter.mode = 'present'
        document.getElementById('slide_viewer').style.display='block'
    }
}

function endPresentation() {
    MDPresenter.mode = 'deck'
    hideSlides()
    updateHash('')
    document.getElementById('slide_viewer').style.display='none'
}

function viewSlide(index) {
    hideSlides()
    startPresentation()
    MDPresenter.currentSlide = index
    let elm = document.querySelectorAll('.slide[data-slide="'+index+'"]')[0]
    elm.className="slide active"
    window.getSelection().empty()
    updateHash('slide'+index)
}

function hideSlides() {
    document.querySelectorAll('.slide.active').forEach(elm => {
        elm.className='slide'
    })
}

function navSlide(incr) {
    let elm = document.querySelector('.slide.active')
    
    // Check for Scroll
    let ok = 1
    if (MDPresenter.nav_scroll) {
        if (incr > 0) {
            if (elm.offsetHeight + elm.scrollTop < elm.scrollHeight) {
                // Scroll Down
                elm.scrollTop = elm.scrollTop+elm.offsetHeight
                ok = 0
            }
        } else {
            if (elm.scrollTop > 0) {
                // Scroll Up
                elm.scrollTop = elm.scrollTop-elm.offsetHeight
                ok = 0
            }
        }
    }
    
    if (ok) {
        // Next slide
        let ind = MDPresenter.currentSlide + incr
        if (ind <= MDPresenter.count && ind > 0) {
            elm.scrollTop = 0
            viewSlide(ind)
        }
    }

}

//---Editor Functions
function toggleEditor() {
    if (document.getElementById('slide_editor').style.display == 'flex') {
        closeEditor()
    } else {
        document.getElementById('slide_editor').style.display='flex'
    }
}

function closeEditor() {
    document.getElementById('slide_editor').style.display='none'
    loadSlides()
}

function saveSlides() {
    let text = document.getElementById('ta_presentation').value
    let a = document.createElement("a");
    let file = new Blob([text], {type: 'text/markdown'});
    a.href = URL.createObjectURL(file);
    a.download = MDPresenter.filename;
    a.click()
    delete a
}

async function loadURL(url) {
    let resp = await fetch(url, {cache:'reload'})
    document.getElementById('ta_presentation').value = await resp.text()
}

async function loadFile() {
    let files = event.target.files
    if (files.length > 0) {
        
        // Load File
        var reader = new FileReader()
        reader.onload = function(file) {
            document.getElementById('ta_presentation').value =file.target.result
            loadSlides()
        }
        reader.onerror = function(){console.log('Error reading file')}
        reader.readAsText(files[0])
    }
}

//---
//---Events
function keyPressHandler(event) {
    // console.log(event)
    let key = event.key
    if (key == 'Escape') {
        endPresentation()
    } else if (key == 'ArrowRight' && MDPresenter.mode == 'present') {
        navSlide(1)
    } else if (key == 'ArrowLeft' && MDPresenter.mode == 'present') {
        navSlide(-1)
    }
}

function hashChange(event) {
    if (MDPresenter.hash_enabled) {
        if (location.hash.startsWith('#slide')) {
            viewSlide(parseInt(location.hash.slice(6)))
        }
    }
}

function updateHash(hash) {
    MDPresenter.hash_enabled = 0
    location.hash=hash
    MDPresenter.hash_enabled = 1
}