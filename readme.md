# MD Presenter
A simple tool to quickly create presentations using markdown.

## [Launch MD Presenter](https://lucidlylogicole.gitlab.io/md_presenter/app/md_presenter.html)

## Features
- create presentation in markdown format
- slide deck and presentation mode
- built in simple editor with save
- slides are delimited by 4 dashes `----`
- optional columns on a slide are delimited by 4 pipes `||||`
- load a presentation by:
    - opening a file  
    or  
    - using the built in editor  
    or
    - by specifying the url `?file=mypresentation.md`
- handles formatting for you
- code advanced features in html if you really need to
- double click on slide in slide deck to open it
- use a footer that is displayed on all slides by:
    - `<footer>MD Presenter Overview</footer>`

## Install
- Just download this folder and open `app/md_presenter.html`

## Examples
- [Help Overview](https://lucidlylogicole.gitlab.io/md_presenter/app/md_presenter.html?file=../examples/overview.md)
- [Sailing Demo Presentation](https://lucidlylogicole.gitlab.io/md_presenter/app/md_presenter.html?file=../examples/sailing.md) - open by default in slide deck mode
- [Sailing Demo Presentation - Slide 2](https://lucidlylogicole.gitlab.io/md_presenter/app/md_presenter.html?file=../examples/sailing.md#slide2) - start on slide 2 in presentation mode

## Resources:
- Icons: [Bootstrap Icons](https://icons.getbootstrap.com/)
- [Commonmark.js](https://github.com/commonmark/commonmark.js) - markdown to html