----
# Sailing

<div style="text-align:center">

![](https://upload.wikimedia.org/wikipedia/commons/7/73/Tarangini.jpg)

## a select summary from Wikipedia

</div>

----
# Sailing

Sailing employs the wind—acting on sails, wingsails or kites—to propel a craft on the surface of the water (sailing ship, sailboat, windsurfer, or kitesurfer), on ice (iceboat) or on land (land yacht) over a chosen course, which is often part of a larger plan of navigation.

A course defined with respect to the true wind direction is called a point of sail.


### 21st Century Sailing
In the 21st century, most sailing represents a form of recreation or sport. Recreational sailing or yachting can be divided into racing and cruising. Cruising can include extended offshore and ocean-crossing trips, coastal sailing within sight of land, and daysailing.

----
# Sail Trimming

- reefing, or reducing the sail area in stronger wind
- altering sail shape to make it flatter in high winds
- raking the mast when going upwind (to tilt the sail towards the rear, this being more stable)
- providing sail twist to account for wind speed differential and to spill excess wind in gusty conditions
- gibbing or lowering a sail

----
# Knots and line handling

The following knots are regarded as integral to handling ropes and lines, while sailing:

- **Bowline** – forms a loop at the end of a rope or line
- **Cleat hitch** – affixes a line to a cleat
- **Clove hitch** – two half hitches around a post or other object
- **Figure-eight** – a stopper knot
- **Half hitch** − a basic overhand knot around a line or object
- **Reef knot** − (or square knot) joins two rope ends of equal diameter
- **Rolling hitch** – a friction hitch to affix a line to itself or another object
- **Sheet bend** – joins to rope ends of unequal diameter

----
# Point of Sail

## Close Hauled
![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Shrike-port-beam.jpg/384px-Shrike-port-beam.jpg)  
the pennant is streaming backwards, the sails are sheeted in tightly.

||||

## Reaching
![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Shrike-reaching.jpg/351px-Shrike-reaching.jpg)  
the pennant is streaming slightly to the side as the sails are sheeted to align with the apparent wind.

----
# Terminology
![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Sailingboat-lightning-num.svg/716px-Sailingboat-lightning-num.svg.png)

||||

1. mainsail
2. staysail
3. spinnaker
4. hull
5. keel
6. rudder
7. skeg
8. mast
9. Spreader
10. shroud

||||

11. sheet
12. boom
13. mast
14. spinnaker pole
15. backstay
16. forestay
17. boom vang


<footer>

Sailing summary from [Wikipedia](https://wikipedia.org/wiki/sailing)

</footer>