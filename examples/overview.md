----
# MD Presenter

A simple markdown presentation tool

<div style="text-align:center">
    
![](icon.png)

</div>

----
# Quickstart

Slides are created by using 4 dashes `---‐` to separate them

    ---‐
    # Slide 1
    
    ---‐
    # Slide 2

- The slides are rendered using [commonmark](https://commonmark.org/) markdown
- the h1 header element is automatically picked up as the slide title

----
# Navigation

- *Double click* on a slide to view it in presentation mode
- *Left* and *right* keyboard arrow keys can navigate back and forth
- *Arrow buttons* in the bottom right corner also navigate
- *Escape* exits the presentation mode
- Link to other slides using the bookmark like: `#slide1`
- auto open a file by specify the file search parameter in the url  
    [?file=examples/sailing.md](?file=examples/sailing.md)

----
# Columns

## Column 1
Split a slide in columns using 4 pipes `|`

More than 2 columns can be added

||||

## Column 2

Example


<div class="codeblock">
    ## Column 1<br><br>
    |||<span>|</span>
    <br><br>
    ## Column 2
</div>


<footer>MD Presenter Overview</footer>

----

# Extra Markdown formatting

## Todo

- [ ] todo
- [x] done
- [-] cancelled

||||

## table

| col1  col2
|--------------
| 1     2
